﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class RegisteredUserInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { private get; set; }
        public UserRoles Role { get; set; }
        public virtual bool CheckPassword(string stringToCheck)
        {
            if (stringToCheck == null)
                throw new ArgumentNullException(nameof(stringToCheck));

            return Password.Equals(stringToCheck);
        }
    }
}
