﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop_Low
{
    public abstract class User
    {
        protected IDBWorker dBWorker;

        public User(IDBWorker dBWorker)
        {
            this.dBWorker = dBWorker;
        }

        public virtual void OpenMenu()
        {
        }

        protected virtual List<Goods> GetGoodsList()
        {
            return dBWorker.GetGoodsList();
        }

        protected virtual Goods GetGoodsByName(string goodsName)
        {
            return dBWorker.GetGoodsByName(goodsName);
        }

        protected virtual bool CreateAccount(string email, string password, string name, UserRoles role)
        {
            return dBWorker.CreateAccount(email, password, name, role);
        }
    }
}
