﻿using System;

namespace ConsoleEShop_Low
{
    public class SecurityService : ISecurityService
    {
        private readonly IDBWorker dBWorker;

        public User DefaultUser { get; }

        public SecurityService(IDBWorker dBWorker)
        {
            this.dBWorker = dBWorker;

            DefaultUser = new GuestUser(dBWorker);

            CurrentUser = DefaultUser;

            dBWorker.CreateAccount("admin@mail", "1", "admin", UserRoles.Admin);
        }

        public User CurrentUser { get; private set; }

        public void Logout()
        {
            Console.WriteLine($"logged out");

            CurrentUser = DefaultUser;
        }

        public bool Login(string login, string password)
        {
            var account = dBWorker.FindAccount(login, password);

            if (account == null)
            {
                return false;
            }

            CurrentUser = SignIn(account);

            return true;
        }

        public bool CreateUser(string email, string password, string name, UserRoles role)
        {
           return dBWorker.CreateAccount(email, password, name, role);
        }

        private User SignIn(RegisteredUserInfo account)
        {
            switch (account.Role)
            {
                case UserRoles.Guest:
                    return DefaultUser;

                case UserRoles.Registered:
                    return new RegisteredUser(dBWorker, account, this);

                case UserRoles.Admin:
                    return new Admin(dBWorker, account, this);

                default: throw new NotImplementedException();
            }
        }
    }
}
