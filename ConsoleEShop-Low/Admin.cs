﻿using System;

namespace ConsoleEShop_Low
{
    public class Admin : RegisteredUser
    {

        public Admin(IDBWorker dBWorker, RegisteredUserInfo account, SecurityService securityService) : base(dBWorker, account, securityService)
        {
        }

        public override void OpenMenu()
        {
            string input;

            do
            {
                Console.WriteLine("Menu for admin user:");
                Console.WriteLine("1 - Create account");
                Console.WriteLine("2 - Find item by name");
                Console.WriteLine("3 - Get list of goods");
                Console.WriteLine("4 - Create order");
                Console.WriteLine("5 - Update personal data");
                Console.WriteLine("6 - View account by id");
                Console.WriteLine("7 - Create goods");
                Console.WriteLine("8 - Change goods");
                Console.WriteLine("9 - Order Status update");
                Console.WriteLine("10 - Orders list");
                Console.WriteLine("11 - Logout");

                input = Console.ReadLine();

                if (!int.TryParse(input, out var cmd))
                {
                    continue;
                }

                switch (cmd)
                {
                    case 1:
                        CreateAccount();
                        break;

                    case 2:
                        FindItemByName();
                        break;

                    case 3:
                        GetItemsList();
                        break;

                    case 4:
                        CreateOrder();
                        break;

                    case 5:
                        Console.WriteLine("Not implemented");
                        break;

                    case 6:
                        GetUserInfo();
                        break;

                   case 7:
                        CreateGoods();
                        break;

                   case 8:
                       Console.WriteLine("Not implemented");
                       break;

                   case 9:
                       UpdateOrderStatus();
                       break;

                   case 10:
                        OrdersHistory();
                       break;

                    case 11:
                       SecurityService.Logout();
                       return;
                }
            }
            while (!string.IsNullOrWhiteSpace(input));
        }

        private void OrdersHistory()
        {
            foreach(var order in dBWorker.GetOrdersHistory())
            {
                Console.WriteLine($"{order.Id} {order.GoodsName}");
            }
        }

        private void UpdateOrderStatus()
        {
            Console.WriteLine("id ?");
            var id = int.Parse(Console.ReadLine());

            Console.WriteLine("Status ?");
            var status = int.Parse(Console.ReadLine());

            dBWorker.UpdateOrderStatus(id, (OrderStatus)status);
        }

        private void CreateGoods()
        {
            var goods = new Goods();

            Console.WriteLine("name ?");
            goods.Name = Console.ReadLine();

            Console.WriteLine("category ?");
            goods.Category = Console.ReadLine();

            Console.WriteLine("descr ?");
            goods.Description = Console.ReadLine();
            
            Console.WriteLine("price ?");
            goods.Price = decimal.Parse(Console.ReadLine());

            dBWorker.CreateGoods(goods);
        }

        private void GetUserInfo()
        {
            Console.WriteLine("id ?");
            var input = Console.ReadLine();
            var account = dBWorker.GetUserInfo(int.Parse(input));

            if (account == null)
            {
                Console.WriteLine("Not found");
                return;
            }

            Console.WriteLine($"{account.Id} {account.Name}, {account.Role}");
        }

        private void CreateOrder()
        {
            Console.WriteLine("Item ?");

            var item = Console.ReadLine();

            dBWorker.CreateNewOrder(item, Account.Id);
        }

        private void GetItemsList()
        {
            foreach (var goods in base.GetGoodsList())
                Console.WriteLine($"{goods.Name}, {goods.Category}, {goods.Description}");

            Console.WriteLine("------------------------------------------------------");
        }

        private void FindItemByName()
        {
            Console.WriteLine("Item name ?");
            var itemName = Console.ReadLine();

            var item = GetGoodsByName(itemName);

            if (item == null)
            {
                Console.WriteLine("Item not found");
            }
            else
            {
                Console.WriteLine($"{item.Name}, {item.Category}, {item.Description}");
            }
        }

        private void CreateAccount()
        {
            Console.WriteLine("email ?");
            var email = Console.ReadLine();

            Console.WriteLine("password ?");
            var pass = Console.ReadLine();

            Console.WriteLine("name ?");
            var name = Console.ReadLine();

            Console.WriteLine("role ? (1 - registed, 2 - admin)");
            var role = Console.ReadLine();

            CreateAccount(email, pass, name, role == "2" ? UserRoles.Admin : UserRoles.Registered);
        }
    }
}
