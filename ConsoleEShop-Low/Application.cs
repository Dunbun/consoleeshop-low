﻿namespace ConsoleEShop_Low
{
    public class Application : IApplication
    {
        static Application ()
        {
            var db = new DBWorker();

            var app = new Application
            {
                SecurityService = new SecurityService(db)
            };

            Instance = app;
        }

        public static IApplication Instance { get; } 

        public ISecurityService SecurityService { get; private set; }
    }
}
