﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop_Low
{
    public class DBWorker : IDBWorker
    {
        private List<Order> _orders;
        private List<RegisteredUserInfo> _users;
        private List<Goods> _goods;
        
        public DBWorker()
        {
            _orders = new List<Order>();
            _users = new List<RegisteredUserInfo>();
            _goods = new List<Goods>();
        }

        public List<Goods> GetGoodsList()
        {
            return _goods;
        }

        public List<Order> GetOrdersHistory()
        {
            return _orders;
        }

        public Goods GetGoodsByName(string goodName)
        {
            if (string.IsNullOrEmpty(goodName))
                throw new ArgumentNullException(nameof(goodName));

            Goods goodsByName = _goods.Where(g => g.Name.Equals(goodName)).FirstOrDefault() ?? null;

            return goodsByName;
        }

        public bool CreateAccount(string email, string password, string name, UserRoles role)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
                throw new ArgumentNullException();

            if (_users.Exists(u => u.Email.Equals(email)))
                return false;

            if (_users.Count == 0)
                _users.Add(new RegisteredUserInfo { Id = 1, Email = email, Password = password.GetHashCode().ToString(), Name = name, Role = role });
            else
                _users.Add(new RegisteredUserInfo { Id = _users.Max(u => u.Id) + 1, Email = email, Password = password.GetHashCode().ToString(), Name = name, Role = role });

            return true;
        }

        public RegisteredUserInfo FindAccount(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                throw new ArgumentNullException();

            var userToLogIn = _users.FirstOrDefault(u => u.Email.Equals(email) && u.CheckPassword(password.GetHashCode().ToString()));

            return userToLogIn;
        }

        public void CreateNewOrder(string goodsName, int userId)
        {
            if (goodsName == null || goodsName.Equals(""))
                throw new ArgumentNullException(nameof(goodsName));

            if (userId == 0)
                throw new ArgumentNullException(nameof(goodsName));

            if (!_goods.Exists(x => x.Name.Equals(goodsName)))
                throw new ArgumentException($"There is no such product name: {goodsName}");

            if (!_users.Exists(u => u.Id == userId))
                throw new ArgumentException($"There is no user with such Id: {userId}");

            Order newOrder = newOrder = new Order { Id = 1, GoodsName = goodsName, Status = OrderStatus.New, UserId = userId };
            
            if (_orders.Count != 0)
                newOrder.Id = _orders.Max(o => o.Id) + 1;

            _orders.Add(newOrder);
        }

        public void UpdateOrderStatus(int orderId, OrderStatus status)
        {
            if (!_orders.Exists(o => o.Id == orderId))
                throw new ArgumentException($"There is no order with such Id: {orderId}");

            _orders.First(o => o.Id == orderId).Status = status;
        }

        public OrderStatus GetOrderStatus(int orderId)
        {
            if (!_orders.Exists(o => o.Id == orderId))
                throw new ArgumentException($"There is no order with such Id: {orderId}");

            return _orders.First(o => o.Id == orderId).Status;
        }

        public RegisteredUserInfo GetUserInfo(int Id)
        {
            if (!_users.Exists(u => u.Id == Id))
                throw new ArgumentException($"There is no user with such Id: {Id}");

            return _users.Where(u => u.Id == Id).First();
        }

        public void ChangeUserName(int userId, string newName)
        {
            if (!_users.Exists(u => u.Id == userId))
                throw new ArgumentException($"There is no user with such Id: {userId}");

            if (string.IsNullOrEmpty(newName))
                throw new ArgumentNullException(nameof(newName));

            _users.Where(u => u.Id == userId).Select(s => { s.Name = newName; return s; }).ToList();
        }

        public void ChangeUserPassword(int userId, string oldPassword, string newPassword)
        {
            if (!_users.Exists(u => u.Id == userId))
                throw new ArgumentException($"There is no user with such Id: {userId}");

            if (string.IsNullOrEmpty(oldPassword))
                throw new ArgumentNullException("Old password can not be null");

            if (string.IsNullOrEmpty(newPassword))
                throw new ArgumentNullException("New password can not be null");

            if (!_users.Exists(u => u.Id == userId && u.CheckPassword(oldPassword.GetHashCode().ToString())))
                throw new ArgumentException("Wrong password");

            _users.Where(u => u.Id == userId && u.CheckPassword(oldPassword.GetHashCode().ToString())).Select(s => { s.Password = newPassword.GetHashCode().ToString(); return s; }).ToList();
        }

        public void ChangeUserEmail(int userId, string newEmail)
        {
            if (!_users.Exists(u => u.Id == userId))
                throw new ArgumentException($"There is no user with such Id: {userId}");

            if (string.IsNullOrEmpty(newEmail))
                throw new ArgumentNullException(nameof(newEmail));

            _users.Where(u => u.Id == userId).Select(s => { s.Email = newEmail; return s; }).ToList();
        }

        public void CreateGoods(Goods goods)
        {
            if (goods == null)
                throw new ArgumentNullException(nameof(goods));

            if (_goods.Exists(g => g.Name.Equals(goods.Name)))
                throw new ArgumentException($"Goods with name {goods} already exists");

            _goods.Add(goods);
        }

        public void ChangeGoodsInfo(Goods goods)
        {
            if (goods == null)
                throw new ArgumentNullException(nameof(goods));

            if (!_goods.Exists(g => g.Name.Equals(goods.Name)))
                throw new ArgumentException($"Goods with name {goods} does not exist");

            _goods.Where(g => g.Name.Equals(goods.Name)).Select(s => { s = goods; return s; }).ToList();
        }
    }
}
